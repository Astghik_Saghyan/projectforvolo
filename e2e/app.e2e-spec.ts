import { VoloProjectPage } from './app.po';

describe('volo-project App', () => {
  let page: VoloProjectPage;

  beforeEach(() => {
    page = new VoloProjectPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
