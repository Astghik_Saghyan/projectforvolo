import { RouterModule} from '@angular/router';
import { NgModule } from '@angular/core';

import { AuthGuard } from '../shared/auth-guard.service';

import { DashboardComponent } from './dashboard.component';
import { UsersComponent } from './users/users.component';
import { EditComponent } from './edit/edit.component';



@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: "",
        redirectTo: '/dashboard',
        pathMatch: 'full',
        canActivate: [AuthGuard]
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
        children: [
                      { path: '', 
                        component: UsersComponent
                      },
                      { path: 'users', 
                      component: UsersComponent, 
                      },
                      { path: "edit/:id", 
                        component: EditComponent, 
                      }                        
                  ],
        canActivate: [AuthGuard]
      }
    ])
  ],
  exports: [RouterModule]
  
})
export class DashRoutingModule { }


