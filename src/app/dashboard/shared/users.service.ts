import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';

import { User } from '../shared/users.model';

@Injectable()
export class UsersService {

  apiUrlUsers: string;
  user:User;

  constructor( private http: Http, @Inject('API_URL_USERS') apiUrlUsers: string ) {
    this.apiUrlUsers = apiUrlUsers;
  } 
    
   
// returns all users of a single page

  getUsers(page) {
   return this.http
      .get( this.apiUrlUsers + '?page=' + page);       
  }

//returns one user's object with the selected id


  getUser(id: number) {
        return this.http
          .get( this.apiUrlUsers+"/"+id); 
    }


/// add user

  addUser() {
    return this.http
      .post( this.apiUrlUsers, {"name": "morpheus", "job": "leader"} )
      .subscribe(data => {
            if (data.status == 201) {alert("User Successfully Added")};
        }, error => {
            console.log(JSON.stringify(error.json()));
        }); 
  }

/// edit user

  editUser(user:User ) {
    return this.http
      .put( this.apiUrlUsers+"/"+user.id , user )
      .subscribe(data => {
            if (data.status == 200) {alert("User Successfully Edited")};
        }, error => {
            console.log(JSON.stringify(error.json()));
        }); 
  }

  /// delet user

  deleteUser(user:User ) {
    return this.http
      .delete( this.apiUrlUsers+"/"+user.id);

      
  }



}
