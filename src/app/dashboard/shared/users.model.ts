export class User {
    id: number;
    first_name: string;
    last_name: string;
    avatar: string;
}

export const TableHeader = [ "ID", "Portrait", "First Name", "Last Name" ];