import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User, TableHeader } from '../shared/users.model';
import { UsersService } from '../shared/users.service'

@Component({
  selector: 'add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})

export class AddComponent implements OnInit {

  tableHeader = TableHeader;

  addUser(){
    this.usersService.addUser();
  }

  constructor(private usersService:UsersService,
        private router:Router) { }

  ngOnInit() {
  }

}
