import { Component, OnInit, Input } from '@angular/core';
import { Router} from '@angular/router';

import { User, TableHeader } from '../shared/users.model';
import { UsersService } from '../shared/users.service';
import { AuthService } from '../../shared/auth.service';
import { PagerService } from '../shared/pager.service';

@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
})
export class UsersComponent implements OnInit {

  usersList:User[];
  tableHeader = TableHeader;
  sortTogID:boolean = true;
  sortTogName:boolean = true;
  sortTogSure:boolean = true;

  // array of all items to be paged
  private totalItems: number;

  // pager object
  pager: any = {};

  constructor( 
  private usersService:UsersService,
  private authService:AuthService, 
  private pagerService:PagerService, 
  private router:Router 
  ) { }

  goPage(page) {
     this.usersService.getUsers(page).subscribe(response => {
            this.usersList = response.json().data;
            this.totalItems = response.json().total;
            this.setPage(page);
        }, error => {
            console.log(JSON.stringify(error.json()));
        }); 
  }

  setPage(page: number) {
      if (page < 1 || page > this.pager.totalPages) {
          return;
      }
      // get pager object from service
      this.pager = this.pagerService.getPager(this.totalItems, page);
  }

  deleteUser(user:User) {
    this.usersService.deleteUser(user).subscribe(data => {
            if (data.status == 204) {alert("User Successfully deleted")}
        }, error => {
            console.log(JSON.stringify(error.json()));
        }); ;
  }

  onSelect(selected:User) {
    this.router.navigate(["dashboard/edit", selected.id]);
  }

  ngOnInit() {
    this.goPage(1);
  }

  //Sorting Functions

  sortId() {
    if ( this.sortTogID == true) {
      this.usersList.sort(function compare(a, b):any {
      return b.id - a.id;
      });
      this.sortTogID = false;
    } else {
      this.usersList.sort(function compare(a, b):any {
      return a.id - b.id;
      });
      this.sortTogID = true;
    }
    
    
  }

  sortName() {
    if (this.sortTogName == true) {
        this.usersList.sort(function compare(a, b):number {
            if (a.first_name>b.first_name) {
              return -1;
            } else if (a.first_name<b.first_name) {
              return 1;
            } else {
              return 0;
            }
          }
        );
     this.sortTogName = false;  
    } else {
      this.usersList.sort(function compare(a, b):any {
            if (a.first_name<b.first_name) {
              return -1;
            } else if (a.first_name>b.first_name) {
              return 1;
            } else {
              return 0;
            }
          }
        );
     this.sortTogName = true; 
     
    }
    
  }

  sortLastName() {
    if (this.sortTogSure == true) {
        this.usersList.sort(function compare(a, b):number {
            if (a.last_name>b.last_name) {
              return -1;
            } else if (a.last_name<b.last_name) {
              return 1;
            } else {
              return 0;
            }
          }
        );
     this.sortTogSure = false;  
    } else {
      this.usersList.sort(function compare(a, b):any {
            if (a.last_name<b.last_name) {
              return -1;
            } else if (a.last_name>b.last_name) {
              return 1;
            } else {
              return 0;
            }
          }
        );
     this.sortTogSure = true; 
     
    }
  }
}
