import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { DashRoutingModule } from './dash-routing.module';

import { DashboardComponent } from './dashboard.component';
import { UsersComponent } from './users/users.component';
import { EditComponent } from './edit/edit.component';
import { AddComponent } from './add/add.component';

@NgModule({
  imports: [
    CommonModule,
    DashRoutingModule,
  ],
  declarations: [
    DashboardComponent, 
    EditComponent, 
    AddComponent,
    UsersComponent
  ],
  exports: [DashboardComponent],
  providers: [{ provide: 'API_URL_USERS', useValue: 'https://reqres.in/api/users' },
  ],

})
export class DashboardModule { }
