import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { User, TableHeader } from '../shared/users.model';
import { UsersService } from '../shared/users.service'

@Component({
  selector: 'edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  tableHeader = TableHeader;
  user:User;
  id:number;

  getUser(id){
    this.usersService.getUser(id).
            subscribe(response => {
                if (response.status == 200) {
                  this.user = response.json().data;
                };
            }, error => {
                console.log(JSON.stringify(error.json()));
            });
  }

  editUser(user:User) {
    this.usersService.editUser(user);
    this.router.navigate(["dashboard/users"]);
  }

  goBack() {
    this.router.navigate(["dashboard/users"]);
  }

  constructor(
    private activatedRoute: ActivatedRoute, 
    private usersService:UsersService,
    private router:Router ) { }

  ngOnInit() {
    this.id = +this.activatedRoute.snapshot.params["id"];
    this.getUser(this.id);
  }

}
