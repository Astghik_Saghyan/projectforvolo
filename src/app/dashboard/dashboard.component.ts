import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';

import { AuthService } from '../shared/auth.service'

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor( 
    private authService:AuthService, 
    private router:Router 
    ) { }

  logOut() {
    this.authService.logOut();
    this.router.navigate(["loge-in"]);
  }
  

  ngOnInit() {

  }

}
