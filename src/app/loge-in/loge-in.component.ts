import { Component, OnInit, Inject } from '@angular/core';
import { AbstractControl, FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Router} from '@angular/router';

import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'app-loge-in',
  templateUrl: './loge-in.component.html',
  styleUrls: ['./loge-in.component.css']
})
export class LogeInComponent implements OnInit {
  logeInForm: FormGroup;
  email:  AbstractControl;
  password:  AbstractControl;
 
  onSubmit(form: any): void {
      this.authService.login(form).subscribe(data => {
              localStorage.setItem("token", data);
              this.router.navigate(["dashboard"]);
          }, error => {
              console.log(JSON.stringify(error.json()));
          });
  }

  constructor( 
    @Inject('API_URL_LOGIN') apiUrlLogin: string,
    private authService:AuthService,
    private router: Router,
    private http: Http, 
    fb: FormBuilder
     ) {
          this.logeInForm = fb.group({
            'email': ['', Validators.required],
            'password': ['', Validators.required]}); 
          this.email = this.logeInForm.controls["email"];
          this.password = this.logeInForm.controls["password"];
     
   }

  ngOnInit() {
  }

}
