import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { DashboardModule } from './dashboard/dashboard.module';

import { LogeInComponent } from './loge-in/loge-in.component';
import { AppComponent } from './app.component';


import { AuthService } from './shared/auth.service';
import { AuthGuard } from './shared/auth-guard.service';
import { UsersService } from './dashboard/shared/users.service';
import { PagerService } from './dashboard/shared/pager.service';

import { PasswordValidatorDirective } from './shared/password-validator.directive';
import { EmailValidatorDirective } from './shared/email-validator.directive';

@NgModule({
  declarations: [
    AppComponent,
    LogeInComponent,
    PasswordValidatorDirective,
    EmailValidatorDirective
  ],
  imports: [
    ReactiveFormsModule,
    AppRoutingModule,
    DashboardModule,
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [
    { provide: 'API_URL_LOGIN', useValue: 'https://reqres.in/api/login' }, 
    AuthService, AuthGuard, UsersService, PagerService
   ],
  bootstrap: [AppComponent]
})
export class AppModule { }
