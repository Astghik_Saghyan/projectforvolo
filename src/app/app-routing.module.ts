import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { LogeInComponent } from './loge-in/loge-in.component';

import { AuthGuard } from './shared/auth-guard.service';

@NgModule({
  imports: [RouterModule.forRoot([
                {
                  path: "",
                  redirectTo: 'dashboard',
                  pathMatch: 'full',
                  canActivate: [AuthGuard]
                },
                {
                  path: 'loge-in',
                  component: LogeInComponent,
                }   
              ])
            ],
  exports: [RouterModule],
  
})
export class AppRoutingModule { }