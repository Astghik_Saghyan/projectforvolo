import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';

import "rxjs/add/operator/map";

@Injectable()
export class AuthService {
  isLoggedIn: boolean;
  apiUrlLogin: string;
  

  login(form) {
   return this.http.post( this.apiUrlLogin, form).map(resp=>{
      return resp.json();
    })  
  }

  logOut() {
    localStorage.setItem("token", "");
  }

  constructor( 
    @Inject('API_URL_LOGIN') apiUrlLogin: string,
    private http: Http
    ) {
      this.apiUrlLogin = apiUrlLogin;
     }

  

}
