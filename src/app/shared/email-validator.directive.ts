import { Directive } from '@angular/core';
import { FormControl, NG_VALIDATORS, ValidatorFn, Validators } from '@angular/forms';


export function emailValidate():ValidatorFn {
  return (control:FormControl):{[key: string]: any} => {
    let EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    const emailValidation = EMAIL_REGEXP.test(control.value);
    return emailValidation?null:{'': ''}
  }
}



@Directive({
  selector: '[emailValidator]',
  providers: [{provide: NG_VALIDATORS, useExisting: EmailValidatorDirective, multi: true}]
})

export class EmailValidatorDirective implements Validators {

  validate(control:FormControl):{[key:string]:any} {  
    return emailValidate()(control);
  }    

  constructor() { }

}


