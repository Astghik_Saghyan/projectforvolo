import { Directive } from '@angular/core';
import { FormControl, NG_VALIDATORS, ValidatorFn, Validators } from '@angular/forms';

export function passwordValidate():ValidatorFn {
  return (control:FormControl):{[key: string]: any} => {
    let PASSWORD_REGEXP = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}/
    const passwordValidation = PASSWORD_REGEXP.test(control.value);
    return passwordValidation?null:{'': ''}
  }
}


@Directive({
  selector: '[passwordValidator]',
  providers: [{provide: NG_VALIDATORS, useExisting: PasswordValidatorDirective, multi: true}]
})

export class PasswordValidatorDirective {
  validate(control:FormControl):{[key:string]:any} {
    return passwordValidate()(control);
  }  

}
